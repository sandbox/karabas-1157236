<?php

function _russian_case_convert($word, $to, $from = 'что') {
	$to = mb_strtolower($to, 'UTF-8');
	$to = preg_replace("/ё/", 'e', $to);
	$to = mb_substr($to, 0, 1, 'UTF-8');

	$w = $word;
	//$w = mb_strtolower($word, 'UTF-8');
	$w .= ' ';
	$w = preg_replace("/ +/", '$ ', $w);
		
	switch ($to) {
		case 'и':
			break;

		case 'р':
			$w = strtr($w, array(
				'ия$' => 'ии',
				'ий$' => 'ого',
				'ая$' => 'ой',
				'ие$' => 'их',
				'я$'  => 'и',
				'й$'  => 'я',
				'а$'  => 'ы',
				'о$'  => 'а',
				'$'   => 'а',
			));
			break;

			
		case 'в':
			$w = strtr($w, array(
				'ия$' => 'ию',
				'ий$' => 'ого',
				'ая$' => 'ую',
				'ие$' => 'ию',
				'а$'  => 'у',
				'$'   => '',
			));
			break;
			
		case 'д':
			$w = strtr($w, array(
				'ия$' => 'ии',
				'ий$' => 'ому',
				'ая$' => 'ой',
				'ие$' => 'им',
				'а$'  => 'е',
				'я$'  => 'е',
				'$'   => 'у',
			));
			break;
		
		case 'т': 
			$w = strtr($w, array(
				'ия$' => 'ией',
				'ий$' => 'им',
				'ая$' => 'ой',
				'ие$' => 'ими',
				'ов$' => 'овым',
				'а$'  => 'ой',
				'$'   => 'ом',
			));
			break;

		case 'п':
			$w = strtr($w, array(
				'ия$' => 'ии',
				'ий$' => 'ом',
				'ая$' => 'ой',
				'ие$' => 'их',
				'а$'  => 'е',
				'$'   => 'е',
			));
			break;
	}
	
	$w = trim(preg_replace("/[ \$]+/", ' ', $w));
	return $w;
}
